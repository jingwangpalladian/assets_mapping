var fs = require('fs');
var source = require('./source').data;
var dest = require('./dest').data;

var map = [];
var mapPath = 'owh-file-map.json';
// var isMatchingFile = function(s, d){
// 	var r = false;
// 	if(s.size === d.size){
// 		if(s.filename.toLowerCase() === d.filename.toLowerCase()){
// 			if(s.ext.toLowerCase() === d.ext.toLowerCase()){
// 				r = true;
// 			}
// 		}
// 	}

// 	return r;
// }

for (var i = 0; i < source.length; i++) {
	var obj = {old_loc:source[i].path,new_loc:'NO MATCH'};
	for (var j = 0; j < dest.length; j++) {		
		if(source[i].size === dest[j].size){
			if(source[i].filename.toLowerCase() === dest[j].filename.toLowerCase()){
				if(source[i].ext.toLowerCase() === dest[j].ext.toLowerCase()){
					obj.new_loc = dest[j].path;
					break;
				}			
			}			
		}
	}
	map.push(obj);
}

fs.writeFile(mapPath, JSON.stringify(map), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("Map file created");
}); 

